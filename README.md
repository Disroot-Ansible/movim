# Movim - Ansible Role

This role covers deployment, configuration and software updates of Movim. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

You can deploy test instance using `Vagrantfile` attached to the role.
```
vagrant up
ansible-playbook -b Playbooks/movim.yml
```

Add `192.168.33.6    movim.example.org` to `/etc/hosts`.

Then you can access cryptpad from your computer on https://movim.example.org

## Playbook
The playbook includes nginx, postgresql and php-fpm roles and deploys entire stack needed to run Movim. Additional role is also available in the Ansible roles repos in git.

## Temporay fix
`account.yml` task is a temporary fix to remove option to change password/remove account. You can use `account` tags like this `ansible-playbook -b Playbooks/movim.yml --tags account` to implement it.
